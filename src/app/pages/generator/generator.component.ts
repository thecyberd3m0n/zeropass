import { PasswordsService } from './../../services/passwords.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  templateUrl: './generator.component.html',
  styleUrls: ['./generator.component.scss']
})
export class GeneratorComponent implements OnInit {
  siteUrl: string;
  username: string;
  password: string;
  constructor(private router: Router, private passwordsService: PasswordsService) { }

  ngOnInit(): void {
  }

  change(pair: { username?: string, siteUrl?: string }) {
    console.log(pair);
    this.passwordsService.derivePassword(pair.siteUrl, pair.username).then((password: string) => {
      this.password = password;
    })
    // todo: if form is valid - derive new password
  }

}
