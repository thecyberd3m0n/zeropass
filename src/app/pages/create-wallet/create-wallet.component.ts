import { PasswordsService } from './../../services/passwords.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  templateUrl: './create-wallet.component.html',
  styleUrls: ['./create-wallet.component.scss']
})
export class CreateWalletComponent implements OnInit {
  constructor(private router: Router) { }
  ngOnInit(): void {
  }

  onFinished(data) {
    // TODO: navigate to passwords section

    this.router.navigate(['generator']);
    // this.secureChatDispatchers.setupProfile(profile);
  }

}
