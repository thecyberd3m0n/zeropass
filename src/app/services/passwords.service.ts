import { CryptoService } from './crypto.service';
import { Injectable } from '@angular/core';


const STORAGE_NAME = 'seed';
@Injectable({
  providedIn: 'root'
})
export class PasswordsService {
  decryptedSeed: string;
  encryptedSeed: string;
  constructor(private cryptoService: CryptoService) { }

  async generateEncryptedSeed(masterKey: string) {
    this.decryptedSeed = this.cryptoService.getRandomString(64);

    this.encryptedSeed = await this.cryptoService.encrypt(this.decryptedSeed, masterKey);
    localStorage.setItem(STORAGE_NAME, this.encryptedSeed);

    return this.encryptedSeed;
  }

  hasSeed(): boolean {
    return !!localStorage.getItem(STORAGE_NAME)
  }

  async derivePassword(url, username) {
    // generate password using lesspass algo
    return await this.cryptoService.derivePassword({site: url, login: username}, this.decryptedSeed);

  }
}
