import { PasswordsService } from './../../services/passwords.service';
import { Component, OnInit } from '@angular/core';
import { CreatorStepLifecycle } from '@genesis-frontend/ux';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-create-wallet-first-step',
  templateUrl: './create-wallet-first-step.component.html',
  styleUrls: ['./create-wallet-first-step.component.scss']
})
export class CreateWalletFirstStepComponent implements OnInit, CreatorStepLifecycle {
  passphrase = '';
  retypePassphrase = '';
  checked = false;
  nextStepSubject = new BehaviorSubject<boolean>(true);
  prevStepSubject = new BehaviorSubject<boolean>(true);
  canGoNext = this.nextStepSubject.asObservable();
  canGoBack = this.prevStepSubject.asObservable();
  error: string = null;
  constructor(private passwordsService: PasswordsService) { }

  ngOnInit(): void {
  }

  validate() {
    this.nextStepSubject.next(this.passphrase.length > 0 && this.checked);
  }

  getStepData() {
    return this.passphrase;
  }
  onNext(): Promise<any> {
    return new Promise(resolve => {
      // encrypt by this passphrase
      this.passwordsService.generateEncryptedSeed(this.passphrase).then((seed) => {
        resolve(seed);
      })
    });
  }

}
