import { TestBed } from '@angular/core/testing';

import { CryptoService } from './crypto.service';

describe('CryptoService', () => {
  let service: CryptoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CryptoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be able to generate random string', () => {
    let random = service.getRandomString(40);
    console.log('random string:', random);
    expect(random.length).toBe(40);
  })

  it('encrypt should not return same string and not same length', async () => {
    const result = await service.encrypt('teststring', 'qwerty123456');
    const leght = result.length;
    expect(result).not.toEqual('teststring');
    expect(leght).not.toEqual(10);
  });

  it('should return decoded text if password is correct', async () => {
    const encrypted = await service.encrypt('teststring', 'qwerty123456');
    const decoded = await service.decrypt(encrypted, 'qwerty123456');
    expect(decoded).toEqual('teststring');
  });

  it('should not return decode text if password bad', async () => {
    const encrypted = await service.encrypt('teststring', 'qwerty123456');
    let decoded = null;
    try {
      decoded = await service.decrypt(encrypted, 'badpass');
    } catch (e) {
      decoded = null;
    }
    console.error(decoded);
    expect(decoded).toEqual(null);
  });
});
