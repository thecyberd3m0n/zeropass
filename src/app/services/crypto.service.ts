import { Injectable } from '@angular/core';

const CHARACTER_SUBSETS = {
  lowercase: "abcdefghijklmnopqrstuvwxyz",
  uppercase: "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
  digits: "0123456789",
  symbols: "!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~"
};

@Injectable({
  providedIn: 'root'
})
export class CryptoService {
  IVFixedField: any;
  lastCounter: any;

  rsCharset = CHARACTER_SUBSETS.uppercase + CHARACTER_SUBSETS.lowercase + CHARACTER_SUBSETS.digits;
  constructor() { }

  getRandomString(length: number): string {
    let arr = new Uint8Array(length);
    window.crypto.getRandomValues(arr);
    arr = arr.map(x => this.rsCharset.charCodeAt(x % this.rsCharset.length));
    return String.fromCharCode.apply(null, arr);
  }

  getSetOfCharacters(rules) {
    if (typeof rules === "undefined") {
      return (
        CHARACTER_SUBSETS.lowercase +
        CHARACTER_SUBSETS.uppercase +
        CHARACTER_SUBSETS.digits +
        CHARACTER_SUBSETS.symbols
      );
    }
    let setOfChars = "";
    rules.forEach(rule => {
      setOfChars += CHARACTER_SUBSETS[rule];
    });
    return setOfChars;
  }

  consumeEntropy(
    generatedPassword,
    quotient,
    setOfCharacters,
    maxLength
  ) {
    let passwordBuilt = generatedPassword;
    if (passwordBuilt.length >= maxLength) {
      return { value: passwordBuilt, entropy: quotient };
    }
    const longDivision = quotient.divmod(setOfCharacters.length);
    passwordBuilt += setOfCharacters[longDivision.remainder];
    return this.consumeEntropy(
      passwordBuilt,
      longDivision.quotient,
      setOfCharacters,
      maxLength
    );
  }

  getOneCharPerRule(entropy, rules) {
    let oneCharPerRules = "";
    let consumedEntropy = entropy;
    rules.forEach(rule => {
      const password = this.consumeEntropy(
        "",
        consumedEntropy,
        CHARACTER_SUBSETS[rule],
        1
      );
      oneCharPerRules += password.value;
      consumedEntropy = password.entropy;
    });
    return { value: oneCharPerRules, entropy: consumedEntropy };
  }

  async encrypt(input, passphrase): Promise<string> {
    const [key, salt] = await this.getKey(passphrase);
    const fixedPart = this.getFixedField();
    const invocationPart = this.getInvocationField();
    const iv = Uint8Array.from([
      ...fixedPart,
      ...new Uint8Array(invocationPart.buffer),
    ]);
    const encryptedData = crypto.subtle.encrypt(
      { name: 'AES-GCM', iv },
      key,
      new TextEncoder().encode(input)
    );

    const encryptedText = await Array.from(
      new Uint8Array(await encryptedData),
      (char) => String.fromCharCode(char)
    ).join('');

    return JSON.stringify([
      btoa(encryptedText),
      Array.from(invocationPart),
      Array.from(salt),
      Array.from(fixedPart)
    ]);
  }

  insertStringPseudoRandomly(initialString, entropy, stringToInsert) {
    let consumedEntropy = entropy;
    let string = initialString;
    for (let i = 0; i < stringToInsert.length; i += 1) {
      const longDivision = consumedEntropy.divmod(string.length);
      string =
        string.slice(0, longDivision.remainder) +
        stringToInsert[i] +
        string.slice(longDivision.remainder);
      consumedEntropy = longDivision.quotient;
    }
    return string;
  }

  async decrypt(encryptedResult, passphrase) {
    let [encryptedData, invocationPart, salt, fixedPart] = JSON.parse(encryptedResult);
    const [key, _] = await this.getKey(passphrase, Uint8Array.from(salt));
    const invocationPartTypedArray = new Uint32Array(1);
    invocationPartTypedArray[0] = invocationPart;

    const iv = Uint8Array.from([
      ...fixedPart,
      ...new Uint8Array(invocationPartTypedArray.buffer),
    ]);
    encryptedData = atob(encryptedData);
    encryptedData = Uint8Array.from(encryptedData.split(''), (char) =>
      String(char).charCodeAt(0)
    );
    const decryptedData = await crypto.subtle.decrypt(
      { name: 'AES-GCM', iv },
      key,
      encryptedData
    );

    const decryptedText = new TextDecoder().decode(
      new Uint8Array(decryptedData)
    );

    return decryptedText;
  }

  async derivePassword(profile: { site: string, login: string }, masterPassword): Promise<string> {
    const { site, login } = profile;
    const salt = site + login;
    const defaultCrypto = { iterations: 100000, keylen: 32, digest: "sha256" };
    const { iterations, keylen, digest } = defaultCrypto;
    const derivedKey = await this.pbkdf2(masterPassword, salt, iterations, keylen, digest);
    // TODO: renderPassword to contain symbols and everything
    // const renderedPassword =
    return derivedKey;
  }


  private async getKey(passphrase, salt = null) {
    passphrase = new TextEncoder().encode(passphrase);
    const digest = await crypto.subtle.digest({ name: 'SHA-256' }, passphrase);
    const keyMaterial = await crypto.subtle.importKey(
      'raw',
      digest,
      'PBKDF2',
      false,
      ['deriveKey']
    );
    if (!salt) {
      salt = crypto.getRandomValues(new Uint8Array(16));
    }
    const key = await crypto.subtle.deriveKey(
      {
        name: 'PBKDF2',
        salt,
        iterations: 100000,
        hash: 'SHA-256',
      },
      keyMaterial,
      { name: 'AES-GCM', length: 256 },
      false,
      ['encrypt', 'decrypt']
    );
    return [key, salt];
  }

  private getFixedField() {
    let value = this.IVFixedField;
    if (value) {
      return Uint8Array.from(JSON.parse(value));
    }
    value = crypto.getRandomValues(new Uint8Array(12));
    this.IVFixedField = JSON.stringify(Array.from(value));

    return value;
  }

  // only for encryption

  private getInvocationField() {
    let counter = this.lastCounter;
    if (counter) {
      counter = Uint32Array.from(JSON.parse(counter));
    } else {
      counter = new Uint32Array(1);
    }
    counter[0]++;
    this.lastCounter = JSON.stringify(Array.from(counter));
    return counter;
  }

  private stringToArrayBuffer(string) {
    const base64String = unescape(encodeURIComponent(string));
    const charList = base64String.split("");
    const arrayBuffer = [];
    for (let i = 0; i < charList.length; i += 1) {
      arrayBuffer.push(charList[i].charCodeAt(0));
    }
    return new Uint8Array(arrayBuffer);
  }

  private arrayBufferToHex(arrayBuffer) {
    const byteArray = new Uint8Array(arrayBuffer);
    let str = "";
    for (let i = 0; i < byteArray.byteLength; i += 1) {
      str += byteArray[i].toString(16).padStart(2, "0");
    }
    return str;
  }

  private pbkdf2(password, salt, iterations, keylen, digest) {
    return window.crypto.subtle
      .importKey("raw", this.stringToArrayBuffer(password), "PBKDF2", false, [
        "deriveKey"
      ])
      .then(key => {
        const algo = {
          name: "PBKDF2",
          salt: this.stringToArrayBuffer(salt),
          iterations,
          hash: 'SHA-512'
        };
        return window.crypto.subtle.deriveKey(
          algo,
          key,
          {
            name: "AES-CTR",
            length: keylen * 8
          },
          true,
          ["encrypt", "decrypt"]
        );
      })
      .then(derivedKey =>
        window.crypto.subtle
          .exportKey("raw", derivedKey)
          .then(keyArray => this.arrayBufferToHex(keyArray))
      );
  }
}
