import { PasswordsService } from './../../services/passwords.service';
import { CreatorStepLifecycle } from '@genesis-frontend/ux';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-create-wallet-second-step',
  templateUrl: './create-wallet-second-step.component.html',
  styleUrls: ['./create-wallet-second-step.component.scss']
})
export class CreateWalletSecondStepComponent implements OnInit, CreatorStepLifecycle {
  nextStepSubject = new BehaviorSubject<boolean>(true);
  prevStepSubject = new BehaviorSubject<boolean>(true);
  canGoNext = this.nextStepSubject.asObservable();
  canGoBack = this.prevStepSubject.asObservable();
  error: string = null;
  checked = false;
  constructor(public passwordsService: PasswordsService) {
  }
  ngOnInit() {
  }

  getStepData() {
    return null;
  }

  onNext(): Promise<any> {
    return new Promise((resolve) => {
      resolve(null);
    })
  }

}
