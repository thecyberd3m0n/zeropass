import { ModuleWithProviders, NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { UxModule } from '@genesis-frontend/ux';
import { AppComponent } from './app.component';
import { MainComponent } from './pages/main/main.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { DesktopLogoWrapperComponent } from './components/desktop-logo-wrapper/desktop-logo-wrapper.component';
import { MatCheckboxModule, MatCommonModule, MatInputModule, MatButtonModule, MatFormFieldModule } from '@angular/material';
import { QRCodeModule } from 'angularx-qrcode';
import { CreateWalletFirstStepComponent } from './components/create-wallet-first-step/create-wallet-first-step.component';
import { CreateWalletComponent } from './pages/create-wallet/create-wallet.component';
import { PasswordsService } from './services/passwords.service';
import { CreateWalletSecondStepComponent } from './components/create-wallet-second-step/create-wallet-second-step.component';
import { TextFieldModule } from '@angular/cdk/text-field';
import { GeneratorComponent } from './pages/generator/generator.component';


export const routes: Routes = [{
  path: '',
  component: AppComponent,
  children: [{
    path: 'home',
    component: MainComponent,
  }, {
    path: 'create-wallet',
    component: CreateWalletComponent,
    children: [
      {
        path: 'create-wallet-first-step',
        component: CreateWalletFirstStepComponent,
        data: { label: 'Create wallet', icon: 'forward' },
      },
      {
        path: 'create-wallet-second-step',
        component: CreateWalletSecondStepComponent,
        data: { label: 'Create wallet', icon: 'forward' },
      },
      { path: '**', redirectTo: 'create-wallet-first-step', pathMatch: 'full' },
    ]
  }, {
    path: 'generator',
    component: GeneratorComponent
  }]
}];

const ModuleRouting: ModuleWithProviders = RouterModule.forRoot(routes, {
  initialNavigation: 'enabled',
  paramsInheritanceStrategy: 'always'
});

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    CreateWalletComponent,
    DesktopLogoWrapperComponent,
    CreateWalletFirstStepComponent,
    CreateWalletSecondStepComponent,
    GeneratorComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    QRCodeModule,
    FlexLayoutModule,
    MatCommonModule,
    MatInputModule,
    MatButtonModule,
    MatCheckboxModule,
    MatFormFieldModule,
    UxModule.forRoot({
      appName: 'ZeroPass'
    }),
    ModuleRouting,
    TextFieldModule,

  ],
  providers: [PasswordsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
